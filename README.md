# Xzibit POC

## Question

`What is a pull request and what can I do with it?`

This question came to be because I was wondering what opening a pull request on a pull request even means.
It happens often at work. We have a cli that lets you download a pull request.
You can then make changes and push to your fork the usual way, and then use
the cli to open a pull request on the pull request...
What is that even? Why am I able to download the pull request in the first place?

I was also wondering if there was some way that I can use this power to automatically
open a PR on a PR that doesn't pass some checks.
(Think code style, trailing white space, no new line at EOF. ..it happens)

## Observation

It turns out that given a PR#, one could checkout it's ref:
https://github.com/genome/docs/wiki/How-do-I-%60git-checkout%60-a-pull-request%3F

## Hypothesis

1. A pull request ref is not actually a branch, but simply the sum of `target` + `source`
2. If I have the latest `PR-ref` and `target`, then `git diff target PR-ref` should be the same as the diff you see in the PR page.

## Experiment

## Data Analysis

## Conclusion